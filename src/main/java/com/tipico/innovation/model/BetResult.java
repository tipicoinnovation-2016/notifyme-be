package com.tipico.innovation.model;

import lombok.Data;

@Data
public class BetResult {

	private int betId;
	private double amountWon;

	public BetResult(int betId, double amountWon){
		this.betId = betId;
		this.amountWon = amountWon;
	}
}
