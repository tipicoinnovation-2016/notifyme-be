package com.tipico.innovation.model;

import lombok.Data;

@Data
public class BetDetails {

	private String gameName;
	private int gameId;
	private int betId;
	private double stake;

}
