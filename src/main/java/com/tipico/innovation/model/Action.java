package com.tipico.innovation.model;

import lombok.Data;

@Data
public class Action {

	private String actionType;
	private int team;

	public Action(String actionType, int team){
		this.actionType = actionType;
		this.team = team;
	}
}
