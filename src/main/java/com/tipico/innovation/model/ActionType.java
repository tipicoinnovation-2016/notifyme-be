package com.tipico.innovation.model;

import lombok.Data;

import java.util.Arrays;

public enum ActionType {

	Penalty(0, "Penalty"),
	Free_Kick(1, "Free Kick"),
	Goal_Kick(2, "Goal Kick"),
	On_Goal(3, "On Goal");

	private int index;
	private String value;

	private ActionType(int index, String value) {
		this.index = index;
		this.value = value;
	}

	public static ActionType getByIndex(int index){
		return Arrays.asList(values()).stream()
			.filter(a -> a.getIndex() == index)
			.findFirst()
			.orElse(On_Goal);
	}

	public int getIndex() {
		return index;
	}

	public String getValue() {
		return value;
	}
}
