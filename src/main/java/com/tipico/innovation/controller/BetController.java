package com.tipico.innovation.controller;

import com.tipico.innovation.model.BetDetails;
import com.tipico.innovation.model.BetResult;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.RestController;

@RestController
@MessageMapping("/bet")
public class BetController {

	@MessageMapping("/live")
	@SendTo("/topic/gameStarted")
	public String live(BetDetails betDetails) throws Exception {
		Thread.sleep(3000);
		return betDetails.getGameName();
	}

	@MessageMapping("/prelive")
	@SendTo("/topic/betWon")
	public BetResult prelive(BetDetails betDetails) throws Exception {
		Thread.sleep(5000);
		return new BetResult(betDetails.getBetId(), betDetails.getStake() * 1.2);
	}


}
