package com.tipico.innovation.controller;

import com.tipico.innovation.model.Action;
import com.tipico.innovation.model.ActionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

@RestController
public class LivePitchController {

	Set<Integer> games = new HashSet<>();
	Random random = new Random(System.currentTimeMillis());

	@Autowired
	SimpMessagingTemplate simpMessagingTemplate;

	@MessageMapping("/livePitch")
	public void live(int gameId) throws Exception {
		games.add(gameId);
	}

	@Scheduled(fixedDelay = 5000)
	public void liveGameActions(){
		games.stream().forEach(gameId -> {
			int team = random.nextInt(2) + 1;
			int actionIndex = random.nextInt(3);
			ActionType actionType = ActionType.getByIndex(actionIndex);
			simpMessagingTemplate.convertAndSend("/topic/events/" + gameId,
				new Action(actionType.getValue(), team));
		});
	}
}
